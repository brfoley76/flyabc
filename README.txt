The repository of data and source code for the Amnat article "A Bayesian approach to social structure uncovers cryptic regulation of group dynamics in Drosophila melanogaster" (Foley et al, 2015).

./code contains the necessary files to run the simulation, including an empirical data file, which is read, line by line, in to initialise each independent genotype*density replicate; the two pieces of the C++ code, and the Python script which writes a functional .cpp script. These scripts were designed to be submitted to independent nodes of a linux cluster, compiled, and run, using Bash scripts.

./processing contains an example output file from the simulation, some python scripts used to process the ABC output, as well as an R script used to analyse the data (in part).
For more info (problems and requests) email brfoley76@gmail.com

