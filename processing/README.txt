./process.py the python script used to parse the ABC output. from this script, the resulting output will have these columns (specified in the R script initial analysis.r):
	sim = the ranseed for a replicate simulation
	it = the number steps in the MCMC chain the output is from
	accepts = the number of times since the last output (ignore this)
	ml = the male leaving rate (should be fixed at 0)
	mj = the male joining rate: The current and subsequent 2 rate paramters need to be divided by 3 to be accurate. I kept them on a scale of -10 to 10 to match the other parameters and to simplify the proposal kernel function, but then in practice divided by 3 
	fl = the female leaving rate
	fj = the female joining rate
	mm = how much males like other males i.e. affinity
	mf = how much males like females 
	fm = how much females like males
	ff = how miuch females like females
	tot.moves = the total number of moves made by any fly in the whole simulation (should be 2000: we tried a bunch of other kinds of time scales, but there was a tendency, even with the sdev variability parameters to favor parameter combinations where one sex got locked in place and rarely moved.)
	m.moves = the number of those moves made by males
	f.moves = the niumber of those moves made by females
	threshold = the acceptance threshold. The desired threshold may not yet have been reached. If it hasn't gotten to a constant state yet, discard all output up till when the fixed threshold is reached
	d2 = the euclidean distance of the current set of summary statistics from 0 (i.e. identity with the empirical target statistics)
	m.ave = this an subsequent statistics have all been transformed and scaled, given the mean and standard deviation of the empirical stats). Male ave (see above for a more complete description of these stats)
	m.var = male var "
	f.ave = female ave
	f.var = female var
	mf.covar = male-female covariance
	mpm = males per male
	mpf = males per female
	fpm = females per male
	fpf = females per female
	mave.sdev = standard deviation among measurements of male ave
	fave.sdev = standard deviation among measurements of female ave

./89.d10.ult.eightparsdev.txt a comma delimited example output file of process.py (for the 89 genotype, density 20)

./intial_analysis.r a bit of r script for reading in the output files (see above) cleaning up, and playing with, the data.
		
