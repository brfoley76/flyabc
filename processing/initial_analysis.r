setwd("/media/Data/Documents/ABC/toGoToAlex");
options(width=180)


####first cleanup, and calculate log.stats

geno.vec=c("ag", "nu", "23", "58", "89", "145");
density.vec=c("d6", "d7", "d8", "d9", "d10");

aggression.vec=c(6.65, 2.631579, 0.3333333, 0.6666667, 1.75, 3);  

#density.vec=c("4_2", "4_3", "5_3") ## , "2", "3", "4", "5", "6");
#aggression.vec=c(0.3333333, 0.6666667, 1.75, 3, 6.65, 2.631579);  ##Julia's new ones.
header.vec=c("sim", "it", "accepts", "ml", "mj", "fl", "fj", "mm", "mf", "fm", "ff", "tot.moves", 
	"m.moves", "f.moves", "threshold", "d2", "m.ave", "m.var", "f.ave", "f.var", "mf.covar", "mpm", "mpf", "fpm", "fpf", "mave.sdev", "fave.sdev");

stat.vec=c("m.ave", "m.var", "f.ave", "f.var", "mf.covar", "mpm", "mpf", "fpm", "fpf")


i=1;
library(fts);
library(mgcv);

cor.tab=c();
slope.tab=c();
coeff.tab=c();

#get rid of all the NA rows, and rows where the simulation seems to have stuttered.
my.cleanup=function(active.file)
{
	m=1;
	p=0;
	line.sum=0;
	n=length(active.file[,1]);
	
	#m.move.vec=c(active.file$m.moves > active.file$f.moves)
	#active.file=active.file[m.move.vec,]
	#f.move.vec=c(active.file$f.moves > active.file$m.moves)
	#active.file=active.file[f.move.vec,]

	####get rid of cases where we're still ratcheting down to the right threshold####
	min.thresh=min(active.file$threshold);
	discard.vec=c(active.file$threshold == min.thresh)

	active.file=active.file[discard.vec,]
}

did.i.getstuck=function(active.file){
  matrix1=active.file[1:(length(active.file[,1])-1),5:11]
  matrix2=active.file[2:(length(active.file[,1])),5:11]
  combomatrix= matrix1==matrix2
  stuckvec=as.numeric(rowSums(combomatrix))
  stuckvec=c(0, stuckvec)
  print(sum(stuckvec))
  active.file=active.file[c(stuckvec == 0),]
}

#############MAIN for Cleanup#####################

i=1
j=1

for (i in 1:length(geno.vec)){
	for (j in 1:length(density.vec)){

		exp.name=paste(geno.vec[i],".",density.vec[j], sep=""); 
		file.name=paste(exp.name, ".ult.eightparsdev.txt", sep="");	
		active.file=read.table(file.name, sep=",", col.names=header.vec, fill=T);
		#file.csv=paste(exp.name, ".eightpar.f.moves.csv", sep="");	
		file.csv=paste(exp.name, ".eightparsdev.csv", sep="");
		active.file=remove.na.rows(active.file);	
		active.file=my.cleanup(active.file);
		#active.file=did.i.getstuck(active.file)
		prop.movesm=c(active.file$m.moves/active.file$tot.moves);
		rel_f_rate=c(log(exp(active.file[,"fj"])/exp(active.file[,"fl"])))

		active.file=cbind(geno.vec[i], density.vec[j], active.file, prop.movesm, rel_f_rate);
		write.csv(active.file, file=file.csv, row.names=F);

		}
	}




##################################################
##################################################
#### Next, make histograms!  YAY! ################



geno.vec=c("ag", "nu", "23", "58", "89", "145");
dens.vec= c("d6", "d7", "d8", "d9", "d10");
tr.vec=c("ml", "mj", "fl", "fj", "mm", "mf", "fm", "ff")      
break.vec=c(0:511)*(23/511)-12.8   

i=1
tr.name.vec=c()
bin.vec=c()
tr.n.vec=c()
for(i in 1:8){
	tr.name.vec=c(tr.name.vec, rep(tr.vec[i], 512))
	bin.vec=c(bin.vec, break.vec)
	tr.n.vec=c(tr.n.vec, rep(i, 512))
	i=i+1
}
hist.table=cbind(tr.name.vec, tr.n.vec, bin.vec)


i=1
colnames.vec=c("tr.name", "tr.n", "bin.val")
results.table=c()
for(i in 1:length(geno.vec)){
	j=1
	for (j in 1:length(dens.vec)){
		col.name=paste(geno.vec[i], dens.vec[j], sep="")
		colnames.vec=c(colnames.vec, col.name)
		file.name=paste(geno.vec[i], ".", dens.vec[j], ".eightparsdev.csv", sep="")		
		active.file=read.csv(file.name)
		
		k=1
		density.results=c()
		for(k in 1:8){

			cat("tr.density=density(active.file$", tr.vec[k], ", from=-12.8, to=10.2)", file="density.source.r", sep="")
			file.tr.density=source("density.source.r")
			file.tr.density=file.tr.density$value
			density.vec=file.tr.density$y
			dens.sum=sum(density.vec)
			density.vec=density.vec/dens.sum


			density.results=c(density.results, density.vec)
		}
	hist.table=cbind(hist.table, density.results)
	}
	
}
#write.csv(hist.table, "smoothed.valid.hist3.csv", row.names=F)
#write.csv(results.table, "mean.median.csv", row.names=F)

colnames(hist.table)=colnames.vec
write.csv(hist.table, "concat.valid.histnew.csv", row.names=F)



#############################################################
############################################################
######ipairs####################################################

library(IDPmisc)

geno.vec=c("ag", "nu", "23", "58", "89", "145");
density.vec=c("d6", "d7", "d8", "d9", "d10");
aggression.vec=c(6.65, 2.631579, 0.3333333, 0.6666667, 1.75, 3);  
i=1
j=1

mode.vec=c()
mean.vec=c()

means=c()
to.plot=c()
for (i in 1:length(geno.vec)){
	for (j in 1:length(density.vec)){
	exp.name=paste(geno.vec[i], ".", density.vec[j], sep=""); 
	file.name=paste(exp.name, ".eightparsdev.csv", sep="");	
	active.file=read.csv(file.name);
	active.file=active.file[,c(7:13, 28, 31)]
	active.subsample = c(1:length(active.file[,1]))
	active.subsample=c((active.subsample %% 5) == 0)
	mean.vec=lapply(active.file[active.subsample,1:9], mean) 
	mean.vec=c(mean.vec, aggression.vec[i])
	mean.vec=as.numeric(mean.vec)
	means=cbind(means, mean.vec)
	ipairs(active.file[active.subsample,1:9])
	pic.name=paste(geno.vec[i], ".", density.vec[j], ".eightparsdev.pic.pdf", sep="")
	dev.copy(pdf, pic.name)
	dev.off()
   }

}


genovec=c(rep("ag", 5), rep("nu", 5),rep("23", 5),rep("58", 5),rep("89", 5),rep("145", 5))
densvec=c(rep(c(6, 7, 8, 9, 10), 6))

mode.table=t(mode.table)
mean.table=t(means)
mode.table=cbind(genovec, densvec, mode.table)
mode.table=cbind(mode.table, mean.table[,11])
mean.table=cbind(genovec, densvec, mean.table)
mean.table=mean.table[,c(1:9, 11)]
headvec=c("genotype", "density", "mj", "fl", "fj", "mm", "mf", "fm", "ff", "aggr")
mode.table=mode.table[,c(1, 2, 4:11)]
colnames(mean.table)=headvec
colnames(mode.table)=headvec

mean.table=as.data.frame(mean.table)
mode.table=as.data.frame(mode.table)



###########################
#####Plot vals over time

i=1
j=1
k=1

par(mfrow=c(2,4))
rep_vec=c(3501:3510)


to.plot=c()
for (i in 1:length(geno.vec)){
	for (j in 1:length(density.vec)){
	exp.name=paste(geno.vec[i], ".", density.vec[j], sep=""); 
	file.name=paste(exp.name, ".eightparsdev.csv", sep="");	
	active.file=read.csv(file.name);
	  for(k in 1:length(rep_vec)){
	    subsamp.vec=c(active.file[,3]==rep_vec[k])
	    active_sub=active.file[subsamp.vec,]
	    plot(active_sub$mj)
	    plot(active_sub$fl)
	    plot(active_sub$fj)
	    plot(active_sub$mm)
	    plot(active_sub$mf)
	    plot(active_sub$fm)
	    plot(active_sub$ff)
	    plot(active_sub$prop.movesm)

	  }
   }

}

###########################
#####Komolgorov Smirnov

i=1
j=1
k=1

rep_vec=c(3501:3530)
trait_vec=c("mj", "fl", "fj", "mm", "mf", "fm", "ff", "prop.movesm")

result_tab=c()
means=c()
to.plot=c()

for (i in 1:length(geno.vec)){
	for (j in 1:length(density.vec)){
	exp.name=paste(geno.vec[i], ".", density.vec[j], sep=""); 
	file.name=paste(exp.name, ".eightparsdev.csv", sep="");	
	active.file=read.csv(file.name);
	
	subsamp.vec=c(active.file[,"sim"]<rep_vec[16])
	subsamp.vec2=c(active.file[,"sim"]>rep_vec[15])
	subsamp1=active.file[subsamp.vec,]
	subsamp2=active.file[subsamp.vec2,]
	par(mfrow=c(4, 4))
	for(k in 1:length(trait_vec)){
	  prune_vec1=c((c(1:length(subsamp.vec) %% 30) == 0))
	  prune_vec2=c((c(1:length(subsamp.vec) %% 30) == 0))
	  aaa=ks.test(subsamp1[prune_vec1, trait_vec[k]], subsamp2[prune_vec2, trait_vec[k]])
	  hist(subsamp1[prune_vec1, trait_vec[k]], xlim=c(-10, 10), breaks=25)
	 hist(subsamp2[prune_vec2, trait_vec[k]], xlim=c(-10, 10), breaks=25)
	  result_line = c(geno.vec[i], density.vec[j], trait_vec[k], aaa$p.value)
	  print(result_line)
	  result_tab=rbind(result_tab, result_line)
	  }

   }

}



##############################################################
############diptest###########################################
##################################################################



geno.vec=c("23", "58", "89", "145", "ag", "nu");
density.vec=c("d7", "d8", "d9", "d10");
i=1
j=1

to.plot=c()
for (i in 1:length(geno.vec)){
	for (j in 1:length(density.vec)){
	exp.name=paste(geno.vec[i],density.vec[j], sep=""); 
	file.name=paste(exp.name, ".sample.csv", sep="");	
	active.file=read.csv(file.name);	
	active.file=active.file[,26:32]
	k=1
	for (k in 1:7){
		
	to.plot=rbind(to.plot, active.file)
	}
	ipairs(to.plot[,1:7])
	pic.name=paste(geno.vec[i], ".pic.pdf", sep="")
	dev.copy(pdf, pic.name)
	dev.off()

}

vexed=c()
for(i in 1:1000){
	aaa=dip(runif(3803))
	vexed=c(vexed, aaa)
	}


vexed=c()
for(i in 1:1000){
 aaa=dip(rnorm(3803))
 vexed=c(vexed, aaa)
}

vexed=sort(vexed)




###############################################################
#####regression of parameters on aggresssion###################

mode.table=read.csv("maxmin_table.csv", row.names=1)


#aggr=c(rep(aggression.vec[1], 4), rep(aggression.vec[2], 4), rep(aggression.vec[3], 4), rep(aggression.vec[4], 4),  rep(aggression.vec[5], 4), rep(aggression.vec[6], 4), aggression.vec)
#model.table=rbind(mode.table, aggr)
#meanl.table=rbind(mean.table, aggr)

mode.table=as.matrix(mode.table)


par(mfrow=c(2, 4))
result.tab=c()
i=1
for(i in 1:7){
	my.model=lm(mode.table[i,1:24]~mode.table[8,1:24])
	plot(mode.table[i,1:24]~mode.table[8,1:24])
	abline(my.model)
	my.modelres=summary(my.model)
	this.result=c(i, summary(my.model)$coefficients[2], my.modelres$adj.r.squared, my.modelres$fstatistic[1], summary(my.model)$coefficients[8])
	result.tab=rbind(result.tab, this.result)
	}


par(mfrow=c(2, 4))
result.tab=c()
i=1
for(i in 1:7){
	my.model=lm(mode.table[i,1:24]~mode.table[8,1:24])
	plot(mode.table[i,1:24]~mode.table[8,1:24])
	abline(my.model)
	my.modelres=summary(my.model)
	this.result=c(i, summary(my.model)$coefficients[2], my.modelres$adj.r.squared, my.modelres$fstatistic[1], summary(my.model)$coefficients[8])
	result.tab=rbind(result.tab, this.result)
	}



par(mfrow=c(2, 4))
result.tab=c()
i=1
for(i in 1:7){
	my.model=lm(model.table[i,25:30]~model.table[8,25:30])
	plot(model.table[i,25:30]~model.table[8,25:30])
	abline(my.model)
	my.modelres=summary(my.model)
	this.result=c(i, summary(my.model)$coefficients[2], my.modelres$adj.r.squared, my.modelres$fstatistic[1], summary(my.model)$coefficients[8])
	result.tab=rbind(result.tab, this.result)
	}


par(mfrow=c(2, 4))
result.tab=c()
i=1
for(i in 1:7){
	my.model=lm(meanl.table[i,25:30]~meanl.table[8,25:30])
	plot(meanl.table[i,25:30]~meanl.table[8,25:30])
	abline(my.model)
	my.modelres=summary(my.model)
	this.result=c(i, summary(my.model)$coefficients[2], my.modelres$adj.r.squared, my.modelres$fstatistic[1], summary(my.model)$coefficients[8])
	result.tab=rbind(result.tab, this.result)
	}


