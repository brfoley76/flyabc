./statsmerge.eightparsdev.txt The empirical data on which the simulations were fit is contained in the file "statsmerge.eightparsdev.txt"
	The space-delimited table columns are as follows (and see "alljobs.eightparsdev.py" where these are parsed)	
		genotype
		number of males (*2 for total density)
		n-reps (the number of empirical measurements we have for this treatment)
		threshold: after calculating a reasonable threshold, I obtained these values. Set to 0 to actually calculate the threshold in the first place.
		male ave = average number of males on each patch, averaged across all reps of that treatment
		male ave sd = standard deviation of male ave across all reps for that treatment
		male var = variance in male number across patches, averaged across reps within treatment (log transformed)
		male var sd = standard deviation of male var across all reps for that treatment
		female ave = see above
		female sd = see above
		female var = see above
		female var sd = see above
		mf covar = covariance between males and females among the patches, averaged across reps within treatment (floor of -2.2, then log transformed)
		mf covar sd = standard deviation of mf covar, across all reps within a treatment
		males per male = number of other males that each male (who is on patch) sees on average (averageed across all reps within treatment, log transformed)		
		males per male sd = sd of males per male, across all reps within a treatment
		females per male = see above
		females per male sd = see above
		males per female = "
		males per female sd = "
		females per female = "
		females per female sd = "
		* there are a bunch of weight variables that we didn't do anything with, yet
		male ave weight = weight we'd give to the male ave summary statistic in the analysis
		female ave weight = as above, so below
		male var weight = "
		female var weight = "
		mf covar weight "
		males per male weight = "
		females per male weight = "
		males per female weight = "
		females per female weight = "
		
./alljobs.eightparsdev.py the python code I use to submit the various jobs. 
	takes as input cchunk1.txt, statsmerge.eightparsdev.txt, cchunk2.eightparsdev.txt
	outputs .cpp ABC and simulation files with different random number seeds and treatment specific variables
	needs to modified to fit whatever file structure you're using
	
./cchunk1.txt
	not really important. just the first few lines of the .cpp file output from alljobs.eightparsdev.py

./cchunk2.eightparsdev.txt
	lots of cool stuff in here. If you set 
	bDoABC=1/0 either run the full ABC analysis, or just simulate data
	bDeterministic=1/0 if ==1, in the function cExperiment::InitializeEvolutionaryParameters(bool bDeterministic), 
		can manually set parameters, then simulate them to test


./mersenne.cpp called by the C++ script, a random number generator. make sure it matches your 32 or 64 bit system, or weird things happen. It turns out.	

./randomc.h another random number thingy

./stdafx.cpp, stdafx.h, targetver.h, tchar.h: libraries and header files for the C++ script. I frankly have no idea what they do, but things seem not to work if they aren't around.S 


	
		
