import sys
import commands
import os
import subprocess
import re
import fileinput

####This script opens up my statistics file, reads each line at a time
##### and creates a new config.xml file. It then submits flysim to the cluster, using the new config file.

#def SubmitTheJob(geno, dens):
	
def WriteTheNewCCode(MyVars, MyOutfile, ranseed):
	totflies=int(MyVars[1])*2
	MyGeno=MyVars[0].replace('\"','')
	MyOutfile.write("const int ranseed=" + str(ranseed) + ";\n") #random number seed
	MyOutfile.write("ofstream ofile(\""+MyGeno + ".d" + MyVars[1] +"."+ str(ranseed) + ".txt\",ios::out);\n") #name of the output (result) file
	MyOutfile.write("const int gciTotalNumberOfFlies=" + str(totflies) + ";\n") #total number of flies in experiment
	MyOutfile.write("const int gciTotalNumberOfMales="+ str(MyVars[1]) + ";\n") #number of those that are male (always 50% in our data)
	MyOutfile.write("const int gciNumberOfPetriDishes= 4;\n") #this never changes, the number of patches. A 5th "patch" is off-patch
	MyOutfile.write("const double dThreshold=" + str(MyVars[3]) + ";\n") #the acceptance threshold. SET THIS TO 0 for the initial, final threshold estimation
	MyOutfile.write("const double dMaleAveAve=" + str(MyVars[4]) + ";\n") #the following are the empirical summary statistics, plus their standard deviations
	#for more info on summary stats, including their calculatons, see the paper.
	#note that many of these have been log transformed for normality
	#see c-code for details. ctrl-F "summary stat transformations"
 	MyOutfile.write("const double dMaleAveSD=" + str(MyVars[5]) + ";\n")
 	MyOutfile.write("const double dMaleVarAve=" + str(MyVars[6]) + ";\n")
 	MyOutfile.write("const double dMaleVarSD=" + str(MyVars[7]) + ";\n")
 	MyOutfile.write("const double dFemaleAveAve=" + str(MyVars[8]) + ";\n")
 	MyOutfile.write("const double dFemaleAveSD=" + str(MyVars[9]) + ";\n")
 	MyOutfile.write("const double dFemaleVarAve=" + str(MyVars[10]) + ";\n")
 	MyOutfile.write("const double dFemaleVarSD=" + str(MyVars[11]) + ";\n")
	MyOutfile.write("const double dMFCovarAve=" + str(MyVars[12]) + ";\n")
	MyOutfile.write("const double dMFCovarSD=" + str(MyVars[13]) + ";\n")
	MyOutfile.write("const double dMpMAve=" + str(MyVars[14]) + ";\n")
	MyOutfile.write("const double dMpMSD=" + str(MyVars[15]) + ";\n")
	MyOutfile.write("const double dFpMAve=" + str(MyVars[16]) + ";\n")
	MyOutfile.write("const double dFpMSD=" + str(MyVars[17]) + ";\n")
	MyOutfile.write("const double dMpFAve=" + str(MyVars[18]) + ";\n")
	MyOutfile.write("const double dMpFSD=" + str(MyVars[19]) + ";\n")
	MyOutfile.write("const double dFpFAve=" + str(MyVars[20]) + ";\n")
	MyOutfile.write("const double dFpFSD=" + str(MyVars[21]) + ";\n")
	
	MyOutfile.write("const double dAveBoysWeight=" + str(MyVars[22]) + ";\n") #We never did anything with these values. 
	#In the future we want to play around with weights for different summary statistics, e.g. to avoid over-weighting due to correlation
	#and to reduce the weight of less informative statistics
	MyOutfile.write("const double dAveGirlsWeight=" + str(MyVars[23]) + ";\n")
	MyOutfile.write("const double dVarBoysWeight=" + str(MyVars[24]) + ";\n")
	MyOutfile.write("const double dVarGirlsWeight=" + str(MyVars[25]) + ";\n")
	MyOutfile.write("const double dCovBoysGirlsWeight=" + str(MyVars[26]) + ";\n")
	MyOutfile.write("const double dMalesPerMaleWeight=" + str(MyVars[27]) + ";\n")
	MyOutfile.write("const double dFemalesPerMaleWeight=" + str(MyVars[28]) + ";\n")
	MyOutfile.write("const double dMalesPerFemaleWeight=" + str(MyVars[29]) + ";\n")
	MyOutfile.write("const double dFemalesPerFemaleWeight=" + str(MyVars[30]) + ";\n")


 	MyOutfile.write("const double dMFCovCorrection= 2.2;\n")
 	##MyOutfile.write("const int ciNoStats= 6;\n")
	MyOutfile.write("const int ciNoStats= 10;\n")
	MyOutfile.write("const int iNSamples=" + str(MyVars[2]) + ";\n")
	return 0;

def MakeTheCPP(i):

	
	MyOutfileName=("autoc."+str(i)+".cpp")
	MyOutfile=open(MyOutfileName, "w")	

	print(MyGeno + "d"+ str(MyDens) + "\n")
	
	MyOutfile.write(FirstBit)
	WriteTheNewCCode(MyVars, MyOutfile, i)
	MyOutfile.write(SecondBit)
	MyOutfile.close()
	return MyOutfileName;	
	

def SubmitTheJob(MyGeno, MyDens, i):
	MakeTheScript(MyGeno, MyDens, i)
	BashTheScript(MyGeno, MyDens, i)
	return 0;

	
def MakeTheScript(MyGeno, MyDens, i):
	MyFolderName=(MyGeno+"d"+MyDens)
	print(MyFolderName)
	#the following should ensure scr.i is empty
	MyDummyName="scr."+str(i)
	MyDummy=open(MyDummyName, "w")
	MyDummy.close()
	MyMakeScrFileName=("makescr"+str(i))
	MyMakeScriptFileLine1=("echo \"cd /home/cmbpanfs-01/bradfole/genotypes/" + MyGeno +  "/"+MyFolderName+"\" >>scr."+str(i))
	MyMakeScriptFileLine2=("echo \"./autosim."+str(i)+".out\">>scr."+str(i))
	MyMakeScriptFile=open(MyMakeScrFileName, "w")
	MyMakeScriptFile.write("#!/bin/bash\n")
	MyMakeScriptFile.write(MyMakeScriptFileLine1+"\n")
	MyMakeScriptFile.write(MyMakeScriptFileLine2+"\n")
	MyMakeScriptFile.close()
	subprocess.call(["chmod", "+x", MyMakeScrFileName])
	subprocess.call(["bash", MyMakeScrFileName])
	
	return 0;
	
def BashTheScript(MyGeno, MyDens, i):
	MySubmitFileName=("submit"+str(i))
	##MySubmissionLine=("qsub -q cmb -N brf."+MyGeno+".d"+MyDens+"."+str(i)+" -l walltime=72:00:00 -l nodes=1:ppn=1 -l mem=500mb -l pmem=500mb -l vmem=500mb "+"scr."+str(i))
	MySubmissionLine=("qsub -q cmb -N brf."+MyGeno+".d"+MyDens+"."+str(i)+" -l walltime=18:00:00 -l nodes=1:ppn=1 -l mem=500mb -l pmem=500mb -l vmem=500mb "+"scr."+str(i))
	MySubmitFile=open(MySubmitFileName, "w")
	MySubmitFile.write("#!/bin/bash\n")
	MySubmitFile.write(MySubmissionLine)
	MySubmitFile.close()
	subprocess.call(["chmod", "+x", MySubmitFileName])
	subprocess.call(["bash", MySubmitFileName])
	
	return 0;
	

#########################################################
### This is the guts of the code. It reads in the beginning and end of the C++ code, cchunk1.txt and cchunk2.eightparsdev.txt
### The eightparsdev designation just means that we played around with a bunch of different numbers of summary statistics (settled on 8)
### and included standard deviation information to scale them properly.
### When it calls WriteTheNewCCode() it writes a new script, with the appropriate variables for that rep (see above)
### and a random number seed 
### it compiles and submits the job


i=3500 # determines the startpoint of ranseed. arbitrary
j=0    # starting index


while(j<300): #determines the number of independent runs for all reps
	MyChunk1=open("cchunk1.txt", "r")  ####the bits of the c code that don't change among reps
	FirstBit=MyChunk1.read()
	MyChunk2=open("cchunk2.eightparsdev.txt", "r")
	SecondBit=MyChunk2.read()
	i=i+1	

	for line in open("statsmerge.eightparsdev.txt", "r"): #scrolls through each rep and submits a job. All these reps have identical ranseeds, but different variables
		j=j+1
		fileline=line
		MyVars=fileline.split()	#see above WriteTheNewCCode(MyVars, MyOutfile, ranseed) where these are described
		MyGeno=MyVars[0].replace('\"','')
		MyDens=MyVars[1]
		os.chdir(MyGeno)
		print(MyGeno + "\n")
		os.chdir(MyGeno + "d"+str(MyDens))
		MyOutfileName=MakeTheCPP(i)
		print(MyOutfileName)
	
		MyCompileFileName=("autosim."+str(i)+".out")
		print(MyCompileFileName)
		subprocess.call(["g++", "-o", MyCompileFileName, MyOutfileName])
		SubmitTheJob(MyGeno, MyDens, i)
		os.chdir("..")
		os.chdir("..")
	

	MyChunk1.close()
	MyChunk2.close()
	
